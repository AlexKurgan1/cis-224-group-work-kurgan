
/*var uniqueRandoms = [];
var numRandoms = 5;
function makeUniqueRandom() {
    // refill the array if needed
    if (!uniqueRandoms.length) {
        for (var i = 0; i < numRandoms; i++) {
            uniqueRandoms.push(i);
        }
    }
    var index = Math.floor(Math.random() * uniqueRandoms.length);
    var val = uniqueRandoms[index];

    // now remove that value from the array
    uniqueRandoms.splice(index, 1);

    return val;

}
*/


$(function () {
    var clickCount;
    var matches, $matches;
    var cardClasses = ["ace", "king", "queen", "jack"];
    var $cards;
    var $resetBtn;
    var pair;
    
    
    $cards = $('#cards div'); 
    $matches = $('#matches');
    $resetBtn = $('#reset');
    
    function init() {
        clickCount = 0;
        matches = Number($matches.text());
        pair = [];
        
        $cards.each(function () {
            let randomClass;
            let num = cardClasses.length; // num is the length of the array 4
            randomClass = cardClasses[
                Math.floor(Math.random()*num)
            ];
            $(this).addClass("_" + randomClass);
        });
        
    }
    
    

    function checkForMatch() {
        let classVal = $(this).attr('class');
        
        function updateClass(div) {
            if (classVal[0] === '_') { 
                classVal = classVal.slice(1);
                div.attr('class', classVal);
                pair.push(classVal);
                clickCount++;
            }
        }
        
        updateClass($(this));
        
        if (clickCount === 2 && pair[0] === pair[1]) {
            matches++;
            $matches.text(matches);
            clickCount = 0;
        }
        
       /* switch(clickCount) {
            case 0:
                updateClass($(this));
                console.log(classVal);
                break;
                
            case 1:
                updateClass($(this));
                console.log(classVal);
                console.log(pair);
                break;
                
            case 2:
                if (pair[0] === pair[1]) {
                    matches++;
                    $matches.text(matches);
                    clickCount = 0;
                    pair = [];
                    //console.log(pair[0]);
                    //console.log(pair[1]);
                } else {
                    
                    
                    clickCount = 0;
                    pair = [];
                }
                    
                
                break;
            default:
                console.log("oh no");
                
        } */
        
        
       
   }
    
    function resetGame() {
        //remove all class attributes
        $cards.each(function () {
            $(this).removeClass();
        });
        
        //re-initialize
        init();
            
        
    }
    
    
    $cards.on('click', checkForMatch);
    $resetBtn.on('click', resetGame);
    init();
   
});